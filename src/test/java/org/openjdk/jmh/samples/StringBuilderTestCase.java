package org.openjdk.jmh.samples;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@RunWith(JUnit4.class)
public class StringBuilderTestCase {

    @Test
    public void test() throws RunnerException {
        Options options = new OptionsBuilder().include(StringBuilderTest.class.getSimpleName())
                .forks(2).build();
        new Runner(options).run();
    }
}
