package com.mxsoft.web;

import java.util.Comparator;

public class TestProgram {
    public static void main(String[] args) {
        String[] arr =  {"ZEBRA", "alligator", "crocodile"};

        System.out.println(findMax(arr, new CaseInsensitiveCompare()));
    }

    public static <AnyType> AnyType findMax(AnyType [] arr, Comparator<? super AnyType> cmp) {
        int maxIndex = 0;

        for(int i = 1; i < arr.length; i ++) {
            if (cmp.compare(arr[i], arr[maxIndex]) > 0) {
                maxIndex = i;
            }
        }

        return arr[maxIndex];
    }
}

class CaseInsensitiveCompare implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return o1.compareToIgnoreCase(o2);
    }
}
