package com.mxsoft.web;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;

public class TestDemo {
    public static void main(String[] args) throws IOException {
//        Set<String> set = new TreeSet<String>();
//
//        set.add("abc");
//        set.add("def");
//        set.add("123");
//        System.out.println(set.size());
//        set.add(null);
//        System.out.println(set.size());
//
//        boolean remove = set.remove(null);
//        System.out.println(remove);


doneWork();

    }

    public static String doneWork() {
        long start = System.currentTimeMillis();
        try {
            URL url = new URL("http://172.16.125.140/threadLocalDemo/helloword");

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();

//            System.out.println("Thread: " + Thread.currentThread().getName() + " -> " + sb);

            return "Thread: " + Thread.currentThread().getName() + " -> " + sb;
        }catch (Exception e) {
//            System.out.println("Thread: " + Thread.currentThread().getName() + " -> " + e.getMessage());

            return "Thread: " + Thread.currentThread().getName() + " -> " + e.getMessage();
        } finally {
            System.out.println("Thread: " + Thread.currentThread().getName() + " -> " + (System.currentTimeMillis() - start));
        }
    }
}
