package com.mxsoft.web;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 模拟高并发
 *
 * @author zhangyingxuan
 */
public class SimulateHighConcurrency1 {
    static volatile int successNum = 0;
    static volatile int failNum = 0;

    public static void main(String[] args) throws InterruptedException {


        final Object obj = new Object();


        synchronized (obj) {


            List<Thread> threadList = new ArrayList<>();

            for (int i = 0; i < 1000; i++) {
                Thread th = new Thread(() -> {
                    try {
                        obj.wait();
                        TimeUnit.SECONDS.sleep(3);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String doneWork = TestDemo.doneWork();
                    if ((doneWork.contains("success"))) {
                        successNum++;
                    } else {
                        failNum++;
                    }
                });
                threadList.add(th);
            }

            for (Thread thread : threadList) {
                thread.start();
            }

            obj.notifyAll();

            for (Thread thread : threadList) {
                thread.join();
            }

            System.out.println("run done: success -> " + successNum + "; fail -> " + failNum);
        }
    }
}
