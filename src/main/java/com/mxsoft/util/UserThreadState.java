package com.mxsoft.util;

import com.mxsoft.web.bean.UserObj;

public class UserThreadState implements ThreadState {
    private UserObj original;
    private UserObj user;

    public UserThreadState(UserObj user) {
        this.user = user;
    }

    @Override
    public void bind() {
        this.original = ThreadContext.getUser();

        ThreadContext.bindUser(this.user);
    }

    @Override
    public void restore() {
        ThreadContext.bindUser(this.original);
    }

    @Override
    public void clear() {
        ThreadContext.unbindUser();
    }
}
