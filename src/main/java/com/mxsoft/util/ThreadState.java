package com.mxsoft.util;

public interface ThreadState {
    void bind();

    void restore();

    void clear();
}
