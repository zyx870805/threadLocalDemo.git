package com.mxsoft.util;

import com.mxsoft.web.bean.UserObj;

public class ThreadContext {
//    private static ThreadLocal<UserObj> userResource = new ThreadLocal<UserObj>();
    private static ThreadLocal<UserObj> userResource = new InheritableThreadLocal<>();

    public static UserObj getUser() {
        return userResource.get();
    }

    public static void bindUser(UserObj userObj) {
        userResource.set(userObj);
    }

    public static UserObj unbindUser() {
        UserObj obj = userResource.get();
        userResource.remove();

        return obj;
    }

}
