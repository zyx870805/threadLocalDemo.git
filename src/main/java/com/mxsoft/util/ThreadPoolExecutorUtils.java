package com.mxsoft.util;

import java.util.concurrent.*;

public class ThreadPoolExecutorUtils {
    private static ThreadPoolExecutor executor;

    static {
        BlockingQueue<Runnable> bqueue = new ArrayBlockingQueue<Runnable>(20);
        executor = new ThreadPoolExecutor(3, 5, 50, TimeUnit.MILLISECONDS, bqueue);
    }

    private ThreadPoolExecutorUtils() {

    }

    public static void execute(Runnable runnable) {
        executor.execute(runnable);
    }

    public static <T> T submit(Callable<T> callable) throws ExecutionException, InterruptedException {
        Future<T> future = executor.submit(callable);

        return future.get();
    }
}
