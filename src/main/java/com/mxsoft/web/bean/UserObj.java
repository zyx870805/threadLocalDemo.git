package com.mxsoft.web.bean;

public class UserObj {
    private String name;
    private Long id;

    public UserObj() {

    }

    public UserObj(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
